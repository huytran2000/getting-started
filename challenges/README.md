# 1. Getting started - Container Challenges

Getting started with container-based challenges on the Joint Cyber Range.

The documentation on [Container challenges workflow](https://docs.jointcyberrange.nl/en/Gebruikersdocumentatie/Challenges/container-challenges-workflow) is probably more up to date.

This folder contains one example container based challenge. Does it work?
